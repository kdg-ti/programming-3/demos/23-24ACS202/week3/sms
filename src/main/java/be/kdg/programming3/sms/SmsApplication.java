package be.kdg.programming3.sms;

import be.kdg.programming3.sms.presentation.View;
import be.kdg.programming3.sms.repository.HardcodedStudentRepository;
import be.kdg.programming3.sms.repository.StudentRepository;
import be.kdg.programming3.sms.service.StudentService;
import be.kdg.programming3.sms.service.StudentServiceImpl;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import java.util.Scanner;

//@Configuration
//@ComponentScan
@SpringBootApplication
public class SmsApplication {

    public static void main(String[] args) {
        var context = SpringApplication.run(SmsApplication.class, args);
        context.getBean(View.class).showMenu();
        context.close();
    }

    @Bean
    public Scanner scanner(){
        return new Scanner(System.in);
    }

  /*  @Bean
    public StudentRepository studentRepository(){
        return new HardcodedStudentRepository();
    }

    @Bean
    public StudentService studentService(StudentRepository studentRepository){
        return new StudentServiceImpl(studentRepository);
    }

    @Bean
    public View view(StudentService studentService){
        return new View(studentService);
    }*/

}
