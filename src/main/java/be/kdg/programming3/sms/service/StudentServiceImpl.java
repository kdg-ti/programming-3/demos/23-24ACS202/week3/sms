package be.kdg.programming3.sms.service;

import be.kdg.programming3.sms.domain.Student;
import be.kdg.programming3.sms.repository.StudentRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class StudentServiceImpl implements StudentService {
    private StudentRepository studentRepository;

    public StudentServiceImpl(StudentRepository studentRepository){
        this.studentRepository = studentRepository;
    }

    @Override
    public Student addStudent(String name, LocalDate birthday, double length){
        return studentRepository.createStudent(new Student(name, birthday, length));
    }

    @Override
    public List<Student> getAllStudents(){
        return studentRepository.readStudents();
    }
}
