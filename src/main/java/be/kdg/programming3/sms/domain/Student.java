package be.kdg.programming3.sms.domain;

import java.time.LocalDate;
import java.util.Objects;

public class Student {
    private int id;
    private String name;
    private LocalDate birthday;
    private double length;

    public Student(String name, LocalDate birthday, double length) {
        this.name = name;
        this.birthday = birthday;
        this.length = length;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public double getLength() {
        return length;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", birthday=" + birthday +
                ", length=" + length +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student student = (Student) o;
        return id == student.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
